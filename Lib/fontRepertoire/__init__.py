__version__ = "0.1.8"


def license() -> str:
    """License of the fontRepertoire package"""
    from importlib.metadata import files
    for file in files("fontRepertoire"):
        if file.name == "LICENSE":
            return file.read_text()
    return ""