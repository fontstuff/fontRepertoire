# fontRepertoire

Simple classes to represent 
- Glyph Repertoire as a set of glyph names.
- Character Set as a set of codepoints (integers).
- Codepoint (integers) to Glyphname (string) maping

## Installation

```shell
pip install fontRepertoire
```

## Documentation

For details read the [Documentation](https://fontstuff.gitlab.io/fontRepertoire).
