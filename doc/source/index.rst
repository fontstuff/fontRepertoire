Welcome to the fontRepertoire documentation!
===============================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   fontRepertoire

Indices and tables
-------------------------------------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
